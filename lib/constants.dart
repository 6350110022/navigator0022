import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFA97CF6);
const kPrimaryLightColor = Color(0xFFE1FAF8);

const double defaultPadding = 16.0;
